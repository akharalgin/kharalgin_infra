#!/bin/bash

# SSH
sudo iptables -A INPUT -p tcp --dport 22 -j ACCEPT -m comment --comment ssh
sudo iptables -A OUTPUT -p tcp --dport 22 -j ACCEPT -m comment --comment ssh

# DNS
sudo iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT -m comment --comment dns
sudo iptables -A OUTPUT -p udp --dport 53 -j ACCEPT -m comment --comment dns

# NTP
sudo iptables -A OUTPUT -p udp --dport 123 -j ACCEPT -m comment --comment ntp

# ICMP
sudo iptables -A OUTPUT -p icmp -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A INPUT -p icmp -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

# OPENVPN
sudo iptables -A INPUT -p udp --dport 1194 -j ACCEPT -m comment --comment openvpn

# loopback
sudo iptables -A OUTPUT -o lo -j ACCEPT
sudo iptables -A INPUT -i lo -j ACCEPT

# HTTP
sudo iptables -A INPUT -p tcp -m multiport --dports 80 -j ACCEPT
sudo iptables -A INPUT -p tcp -m multiport --dports 443 -j ACCEPT

sudo iptables -A OUTPUT -p tcp -m multiport --dports 80 -j ACCEPT
sudo iptables -A OUTPUT -p tcp -m multiport --dports 443 -j ACCEPT

# ESTABLISHED SESSIONS
sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# INVALID
sudo iptables -A OUTPUT -m state --state INVALID -j DROP
sudo iptables -A INPUT -m state --state INVALID -j DROP

# DROP ALL
sudo iptables -P OUTPUT DROP
sudo iptables -P INPUT DROP
sudo iptables -P FORWARD DROP