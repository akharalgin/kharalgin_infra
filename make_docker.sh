#!/bin/bash

docker build -t akharalgin/kharalgin_infra:proxy ./proxy/
#docker push akharalgin/kharalgin_infra:proxy

docker build -t akharalgin/kharalgin_infra:web ./web/
#docker push akharalgin/kharalgin_infra:web

docker build -t akharalgin/kharalgin_infra:mon ./mon/
#docker push akharalgin/kharalgin_infra:mon

docker compose up -d
